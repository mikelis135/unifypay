package com.usehover.hovertest.model

enum class TransactionTypes {
    AIRTIME,
    DATA,
    TRANSFER
}